package com.example.gameapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.TextView
import java.lang.reflect.Field




class MainActivity : AppCompatActivity() {

    private lateinit var timeLeftTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                timeLeftTextView.text ="Seconds" + millisUntilFinished / 1000
            }

            override fun onFinish() {
                timeLeftTextView.text = "Done"
            }
        }.start()


    }

}
